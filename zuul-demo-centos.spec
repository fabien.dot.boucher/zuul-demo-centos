Summary: A demo
Name: zuul-demo-centos
Version: 1.0.0
Release: centos
License: Apache-2
URL: https://pagure.io/zuul-distro-jobs
Source0: HEAD.tgz
BuildArch: noarch

%description
Just a demo of Zuul workflow around rpm build

%prep

%build

%install
echo "Hello Zuul" > README.md


%files
%defattr(-,root,root,-)
%doc README.md


%changelog
* Thu Mar  4 2021 Tristan Cacqueray <tdecacqu@redhat.com> - demo-centos
- Initial build.
